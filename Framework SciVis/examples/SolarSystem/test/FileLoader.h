#ifndef FILELOADER
#define FILELOADER
#include "Vector2.h"
#include <vector>
#include <string>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <array>

class FileLoader
{

public: 
	FileLoader(){
	this->max_xdefPoints = -1000;
	this->max_xinitPoints = -1000;
	this->min_xinitPoints = 1000;
	this->max_xdefPoints = 1000;
	this->max_ydefPoints = -1000;
	this->max_yinitPoints = -1000;
	this->min_yinitPoints = 1000;
	this->max_ydefPoints = 1000;
	this->min_xdefValue = 1000;
	this->max_xdefValue = -1000;
	this->min_ydefValue = 1000;
	this->max_ydefValue = -1000;
	this->min_stressx = 1000;
	this->max_stressx = -1000;
	};

	std::vector<glm::vec2> displacement_vectors;
	std::vector<float> displacement_values;
	std::vector<int> triangles;
	std::vector<float> renderPosition;
	std::vector<float> initPoints;
	std::vector<float> stressx;

	float min_xinitPoints;
	float max_xinitPoints;
	float min_xdefPoints;
	float max_xdefPoints;
	float min_yinitPoints;
	float max_yinitPoints;
	float min_ydefPoints;
	float max_ydefPoints;
	float min_xdefValue;
	float max_xdefValue;
	float min_ydefValue;
	float max_ydefValue;
	float min_stressx;
	float max_stressx;

	void readDisplacementValues(std::string filePath);
	void readDeformedPoints(std::string filePath);
	void readTriangles(std::string filePath);
	void readInitPoints(std::string filePath);
	void readStresses(std::string filePath);
	void getMinCoord_initPoints();
	void getMaxCoord_initPoints();
	void getMinCoord_defPoints();
	void getMaxCoord_defPoints();

};

#endif