//////////////////////////////////////////////////////////////////////////
//Sources: http://openglbook.com/
////////// http://www.dhpoware.com/demos/glObjViewer.html
////////// http://www.arcsynthesis.org/gltut/
/////////////////////////////////////////////////////////////////////////

//Assignment 1 Computergrafik
//Code of Oana-Iuliana Popescu, 112331 

#define GLEW_STATIC

#include <iostream>
#include <climits>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include "ColorMapper.h"
#include <algorithm>
#include "FileLoader.h"
#include "TextureLoader.h"
#include <algorithm>

#include <TransformationStack.h>
#include <Shader.h>
//include gloost::Mesh wich is a geometry container
#include <Mesh.h>
//mesh object for loading planet geometry
gloost::Mesh* mesh = 0;

// loader for the wavefront *.obj file format
#include <ObjLoader.h>
#include <ctime>
#include <cstdlib> //rand and srand
//#include<FreeImage.h>

#define PI 3.1415926535897932384626433832795028841971


//
float renderMagnification = 1/ (2 / (0.3 / 18));
FileLoader load;
std::vector<float> renderPositions;
std::vector<float> renderPositionsAndValues;
std::vector<float> initPoints;
std::vector<float> displacementValues;
std::vector<int> triangles; 
std::vector<float> stressx;

int windowWidth = 800;
int windowHeight = 600;
int windowHandle = 0;

//initial camera position
float camera_position[] = {0.0f, 0.0, 8.0f};

bool middleMouseButtonDown = false;
bool leftMouseButtonDown = false;
bool rightMouseButtonDown = false;

unsigned frameCount = 0;

//handles for shader variables
unsigned projectionMatrixUniformLocation = 0;
unsigned modelMatrixUniformLocation  = 0;
unsigned viewMatrixUniformLocation = 0;
unsigned normalMatrixUniformLocation     = 0;

unsigned colorUniformLocation = 0;
unsigned colorTextureUniformLocation = 0;

//handles for all sort of geometry objects
unsigned vertexArrayObject = 0;
unsigned vertexBufferObject = 0;
unsigned elementArrayBuffer = 0;

//handles for shader program and shaders
unsigned shaderProgram = 0;
unsigned vertexShader = 0;
unsigned fragmentShader = 0;

//handlesfor frame buffer object
unsigned fbo = 0;
unsigned colorBuffer = 0;
unsigned depthBuffer = 0;

//colorbar texture
unsigned colorbar = 0;

//the three different matrices for projection, viewing and model transforming

TransformationStack modelTransformationStack;
TransformationStack cameraTransformationStack;

//Function callbacks
void initialize(int, char*[]);
void initWindow(int, char*[]);
void resizeFunction(int, int);
void idleFunction(void);

//forward declaration of functions
void mouseInput(int button, int state, int x, int y);
void specialKeyRelease(int keyEvent, int x, int y);
void specialKeyPress(int keyEvent, int x, int y);
void keyRelease(unsigned char keyEvent, int x, int y);
void keyPress(unsigned char keyEvent, int x, int y);

void timerFunction(int);
void cleanup(void);
void loadModel(void);
void loadTexture(void);
void setupShader();
void draw(void);
void renderFunction(void);
void setupFBO(void);
void resizeFBOTextures(void);

void drawCircles(glm::mat4 const& model_matrix, glm::mat4 const& view_matrix);

unsigned circlesVertexArrayObject = 0;
unsigned circlesVertexBufferObject[2];
unsigned circlesElementsBufferObject = 0;

unsigned trianglesVertexArrayObject = 0;

unsigned circlesShaderProgram = 0;
unsigned circlesVertexShader = 0;
unsigned circlesFragmentShader = 0;


unsigned circlesShaderProjectionMatrixUniformLocation = 0;
unsigned circlesShaderModelMatrixUniformLocation = 0;
unsigned circlesShaderViewMatrixUniformLocation = 0;

unsigned initPVertexArrayObject = 0;
unsigned initPVertexBufferObject = 0;
unsigned initPElementsBufferObject = 0;

unsigned initPShaderProjectionMatrixUniformLocation = 0;
unsigned initPShaderModelMatrixUniformLocation = 0;
unsigned initPShaderViewMatrixUniformLocation = 0;

unsigned defPVertexArrayObject = 0;
unsigned defPVertexBufferObject = 0;
unsigned defPElementsBufferObject = 0;

unsigned defPShaderProjectionMatrixUniformLocation = 0;
unsigned defPShaderModelMatrixUniformLocation = 0;
unsigned defPShaderViewMatrixUniformLocation = 0;

unsigned meshShaderProgram = 0;
unsigned meshVertexShader = 0;
unsigned meshFragmentShader = 0;

unsigned defmeshShaderProgram = 0;
unsigned defmeshVertexShader = 0;
unsigned defmeshFragmentShader = 0;

void drawInitMesh(glm::mat4 const& model_matrix, glm::mat4 const& view_matrix);
void drawDeformedMesh(glm::mat4 const& model_matrix, glm::mat4 const& view_matrix);

/////////////////////////////////////////////////////////////////////////////////////////
float map(float x, float in_min, float in_max, float out_min, float out_max);
//void mapCoordToScreenCoord(std::vector<float>& coords, float min, float max);

float map(float x, float in_min, float in_max, float out_min, float out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}



int main(int argc, char* argv[])
{
	initialize(argc, argv);

	//start the glut event system
	glutMainLoop();

	exit(EXIT_SUCCESS); 
}


/////////////////////////////////////////////////////////////////////////////////////////


//called every frame this functions draw
void draw(void) 
{

	cameraTransformationStack.clear();

	//translate the camera in positive z direction to actually see the geometry residing in the coordinate origin 
    cameraTransformationStack.pushMatrix(glm::translate(glm::mat4(1.0), glm::vec3(camera_position[0], camera_position[1], camera_position[2]) ) );

	//invert the camera transformation to move the vertices
	glm::mat4 viewMatrix = glm::inverse(cameraTransformationStack.topMatrix() );

	//upload the view matrix to the currently bound shader
	glUniformMatrix4fv(viewMatrixUniformLocation, 1, GL_FALSE,  glm::value_ptr(viewMatrix) );

	//clear the transformation stack
	modelTransformationStack.clear();

	glBindVertexArray(0);
	glUseProgram(0);

	//##
	//some stars
	//now use the star shader program
	glUseProgram(circlesShaderProgram);
	//upload the view matrix also for the star shader program
	glUniformMatrix4fv(circlesShaderViewMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));

	//scale the unit star cube up to the range of -10^3 to 10^3
	modelTransformationStack.pushMatrix(glm::scale(glm::mat4(1.0f), glm::vec3(10.0)));
	//have a look inside of the function to see the binding and draw call for the star geometry
	drawCircles(modelTransformationStack.topMatrix(), viewMatrix);
	//reset our transformations
	

	glUseProgram(meshShaderProgram);
	glUniformMatrix4fv(initPShaderViewMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));
	drawInitMesh(modelTransformationStack.topMatrix(), viewMatrix);

	glUseProgram(defmeshShaderProgram);
	glUniformMatrix4fv(defPShaderViewMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));
	drawDeformedMesh(modelTransformationStack.topMatrix(), viewMatrix);

	modelTransformationStack.clear();
}

/////////////////////////////////////////////////////////////////////////////////////////
void mouseInput(int button, int state, int x, int y)
{
  //handle mouse input here
}

/////////////////////////////////////////////////////////////////////////////////////////
void specialKeyRelease(int keyEvent, int x, int y)
{
	//handle key release events of special keys here (like arrow keys)
}

/////////////////////////////////////////////////////////////////////////////////////////
void specialKeyPress(int keyEvent, int x, int y)
{
	//handle key press events of special keys here (like arrow keys)
}

/////////////////////////////////////////////////////////////////////////////////////////
void keyRelease(unsigned char keyEvent, int x, int y)
{
	if(keyEvent == 'f' || keyEvent == 'F')
	{
		glutFullScreenToggle();
	}
    if(keyEvent == 27) //escape key
	{
		cleanup();
		glutExit();
	}
	if(keyEvent == 'w' || keyEvent == 'W')
	{
		camera_position[2] -= 1.0;
	}
	if(keyEvent == 's' || keyEvent == 'S')
	{
		camera_position[2] += 1.0;
	}
	//up
	if(keyEvent == 'q' || keyEvent == 'Q')
	{
		//std::cout<<"up key";
		camera_position[1] -= 1.0;
	}
	//down
	if(keyEvent == 'a' || keyEvent == 'A')
	{
		//std::cout<<"down key";
		camera_position[1] += 1.0;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
void keyPress(unsigned char keyEvent, int x, int y)
{}




/////////////////////////////////////////////////////////////////////////////////////////
void timerFunction(int value){
	if(0 != value)
	{
		int fps = frameCount * 4;
		glutSetWindowTitle( (gloost::toString(fps) + " fps").c_str());

	}
	frameCount = 0;
	glutTimerFunc(250, timerFunction, 1);
}


void renderFunction(void)
{
	++frameCount;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	draw();

	glutSwapBuffers();
	glutPostRedisplay();
}


/////////////////////////////////////////////////////////////////////////////////////////


void setupShader()
{
	circlesShaderProgram = glCreateProgram();
	{
		//load a shader (of the given type) and compile it in the convenience class 'Shader'
		circlesVertexShader = Shader::loadShader("../../../data/shaders/starVertexShader.vs", GL_VERTEX_SHADER);
		circlesFragmentShader = Shader::loadShader("../../../data/shaders/starFragmentShader.fs", GL_FRAGMENT_SHADER);

		//attach the different shader components to the shader program ...
		glAttachShader(circlesShaderProgram, circlesVertexShader);
		glAttachShader(circlesShaderProgram, circlesFragmentShader);
	}
	//... and compile it
	glLinkProgram(circlesShaderProgram);

	//program is linked, so we can detach compiled shaders again
	glDetachShader(circlesShaderProgram, circlesVertexShader);
	glDetachShader(circlesShaderProgram, circlesFragmentShader);

	//obtain shader variable locations
	circlesShaderModelMatrixUniformLocation = glGetUniformLocation(circlesShaderProgram, "ModelMatrix");
	circlesShaderViewMatrixUniformLocation = glGetUniformLocation(circlesShaderProgram, "ViewMatrix");
	circlesShaderProjectionMatrixUniformLocation = glGetUniformLocation(circlesShaderProgram, "ProjectionMatrix");

	colorTextureUniformLocation = glGetUniformLocation(shaderProgram, "colorTexture");

	meshShaderProgram = glCreateProgram();
	{
		//load a shader (of the given type) and compile it in the convenience class 'Shader'
		meshVertexShader = Shader::loadShader("../../../data/shaders/meshVertexShader.vs", GL_VERTEX_SHADER);
		meshFragmentShader = Shader::loadShader("../../../data/shaders/meshFragmentShader.fs", GL_FRAGMENT_SHADER);

		//attach the different shader components to the shader program ...
		glAttachShader(meshShaderProgram, meshVertexShader);
		glAttachShader(meshShaderProgram, meshFragmentShader);
	}
	//... and compile it
	glLinkProgram(meshShaderProgram);

	//program is linked, so we can detach compiled shaders again
	glDetachShader(meshShaderProgram, meshVertexShader);
	glDetachShader(meshShaderProgram, meshFragmentShader);

	initPShaderModelMatrixUniformLocation = glGetUniformLocation(		meshShaderProgram, "ModelMatrix");
	initPShaderViewMatrixUniformLocation = glGetUniformLocation(		meshShaderProgram, "ViewMatrix");
	initPShaderProjectionMatrixUniformLocation = glGetUniformLocation(	meshShaderProgram, "ProjectionMatrix");

	defmeshShaderProgram = glCreateProgram();
	{
		//load a shader (of the given type) and compile it in the convenience class 'Shader'
		defmeshVertexShader = Shader::loadShader("../../../data/shaders/defmeshVertexShader.vs", GL_VERTEX_SHADER);
		defmeshFragmentShader = Shader::loadShader("../../../data/shaders/defmeshFragmentShader.fs", GL_FRAGMENT_SHADER);

		//attach the different shader components to the shader program ...
		glAttachShader(defmeshShaderProgram, defmeshVertexShader);
		glAttachShader(defmeshShaderProgram, defmeshFragmentShader);
	}
	//... and compile it
	glLinkProgram(defmeshShaderProgram);

	//program is linked, so we can detach compiled shaders again
	glDetachShader(defmeshShaderProgram, defmeshVertexShader);
	glDetachShader(defmeshShaderProgram, defmeshFragmentShader);

	defPShaderModelMatrixUniformLocation = glGetUniformLocation(		defmeshShaderProgram, "ModelMatrix");
	defPShaderViewMatrixUniformLocation = glGetUniformLocation(			defmeshShaderProgram, "ViewMatrix");
	defPShaderProjectionMatrixUniformLocation = glGetUniformLocation(	defmeshShaderProgram, "ProjectionMatrix");
}


/////////////////////////////////////////////////////////////////////////////////////////


void loadModel()
{
	//load data model
	load.readDeformedPoints("D:/repos/visualisierung_scivis/Framework SciVis/data/files/TABDEL.txt");
	load.readTriangles("D:/repos/visualisierung_scivis/Framework SciVis/data/files/triangleList.txt");
	load.readInitPoints("D:/repos/visualisierung_scivis/Framework SciVis/data/files/initPoints.txt");
	load.readDisplacementValues("D:/repos/visualisierung_scivis/Framework SciVis/data/files/defevalf.txt");
	load.readStresses("D:/repos/visualisierung_scivis/Framework SciVis/data/files/stressx.txt");
	renderPositions = load.renderPosition;
	initPoints = load.initPoints;
	displacementValues = load.displacement_values;
	stressx = load.stressx;
	
	for (auto i = initPoints.begin(); i != initPoints.end(); i = i + 2)
	{
		(*i) = map((*i), load.min_xinitPoints, load.max_xinitPoints, -0.9, 0.9);
	}
	
	for (int i = 1; i <= initPoints.size(); i = i + 2)
	{
		initPoints[i] = map(initPoints[i], load.min_yinitPoints, load.max_yinitPoints, -0.9, 0.9);
	}

	for (auto i = renderPositions.begin(); i != renderPositions.end(); i = i + 2)
	{
		(*i) = map((*i), load.min_xinitPoints, load.max_xinitPoints, -0.9, 0.9);
	}
	
	for (int i = 1; i <= renderPositions.size(); i = i + 2)
	{
		renderPositions[i] = map(renderPositions[i], load.min_yinitPoints, load.max_yinitPoints, -0.9, 0.9);
	}

	std::transform(stressx.begin(), stressx.end(), stressx.begin(), [](float x){ return 1.0; });
	
	triangles = load.triangles;

	//generate a new VertexArrayObject. This describes our BufferLayout
	glGenVertexArrays(1, &circlesVertexArrayObject);
	//bind it to be our active VAO
	glBindVertexArray(circlesVertexArrayObject);

	//generate a new VertexBufferObject. This will become the GPU representation of our CPU data
	glGenBuffers(2, circlesVertexBufferObject);
	//bind it to be our active VBO
	glBindBuffer(GL_ARRAY_BUFFER, circlesVertexBufferObject[0]);

	glGenBuffers(1, &circlesElementsBufferObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, circlesElementsBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * triangles.size(),  &(triangles[0]), GL_STATIC_DRAW);

	//---------------
	//load the data of our CPU buffer via the handle we just created into our GPU Buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * renderPositions.size(),  &(renderPositions[0]), GL_STATIC_DRAW);
	
	//DEFINE TEX COORD POSITION IN LAYOUT
	//enable vertex attribute at location 0 (compare with vertex shader input)
	glEnableVertexAttribArray(0); //this will describe our position

	//specify where to read the data for attribute at location 0 
	glVertexAttribPointer(0, //first attribute
		2, //how many components for the position?
		GL_FLOAT, //datatype 
		GL_FALSE, //should the data be normalized?
		sizeof(float)* 2, //size of whole vertex attribute stride for one primitive (3 * float for position + 3 * float for color)
		(GLvoid*)(0)); //offset in stride: position is begins at byte 0 in the stride. expected to be cast to GLvoid*

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//---------------
	glBindBuffer(GL_ARRAY_BUFFER, circlesVertexBufferObject[1]);
	//load the data of our CPU buffer via the handle we just created into our GPU Buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * load.stressx.size(),  &(load.stressx[0]), GL_STATIC_DRAW);
	
	//DEFINE TEX COORD POSITION IN LAYOUT
	//enable vertex attribute at location 0 (compare with vertex shader input)
	glEnableVertexAttribArray(1); //this will describe our position

	//specify where to read the data for attribute at location 0 
	glVertexAttribPointer(1, //first attribute
		1, //how many components for the position?
		GL_FLOAT, //datatype 
		GL_FALSE, //should the data be normalized?
		sizeof(float)* 1, //size of whole vertex attribute stride for one primitive (3 * float for position + 3 * float for color)
		(GLvoid*)(0)); //offset in stride: position is begins at byte 0 in the stride. expected to be cast to GLvoid*

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	


	//INIT MESH
		//generate a new VertexArrayObject. This describes our BufferLayout
	glGenVertexArrays(1, &initPVertexArrayObject);
	//bind it to be our active VAO
	glBindVertexArray(initPVertexArrayObject);

	//generate a new VertexBufferObject. This will become the GPU representation of our CPU data
	glGenBuffers(1, &initPVertexBufferObject);
	//bind it to be our active VBO
	glBindBuffer(GL_ARRAY_BUFFER, initPVertexBufferObject);

	glGenBuffers(1, &initPElementsBufferObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, initPElementsBufferObject);
	//---------------


	//load the data of our CPU buffer via the handle we just created into our GPU Buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * initPoints.size() ,  &(initPoints[0]), GL_STATIC_DRAW);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * triangles.size(),  &(triangles[0]), GL_STATIC_DRAW);

	
	//DEFINE POSITION IN LAYOUT
	//enable vertex attribute at location 0 (compare with vertex shader input)
	glEnableVertexAttribArray(0); //this will describe our position

	//specify where to read the data for attribute at location 0 
	glVertexAttribPointer(0, //first attribute
		2, //how many components for the position?
		GL_FLOAT, //datatype 
		GL_FALSE, //should the data be normalized?
		sizeof(float)* 2, //size of whole vertex attribute stride for one primitive (3 * float for position + 3 * float for color)
		(GLvoid*)(0)); //offset in stride: position is begins at byte 0 in the stride. expected to be cast to GLvoid*


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//DEFORMED MESH
		//generate a new VertexArrayObject. This describes our BufferLayout
	glGenVertexArrays(1, &defPVertexArrayObject);
	//bind it to be our active VAO
	glBindVertexArray(defPVertexArrayObject);

	//generate a new VertexBufferObject. This will become the GPU representation of our CPU data
	glGenBuffers(1, &defPVertexBufferObject);
	//bind it to be our active VBO
	glBindBuffer(GL_ARRAY_BUFFER, defPVertexBufferObject);

	glGenBuffers(1, &defPElementsBufferObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, defPElementsBufferObject);
	//---------------


	//load the data of our CPU buffer via the handle we just created into our GPU Buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * renderPositions.size() ,  &(renderPositions[0]), GL_STATIC_DRAW);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * triangles.size(),  &(triangles[0]), GL_STATIC_DRAW);

	
	//DEFINE POSITION IN LAYOUT
	//enable vertex attribute at location 0 (compare with vertex shader input)
	glEnableVertexAttribArray(0); //this will describe our position

	//specify where to read the data for attribute at location 0 
	glVertexAttribPointer(0, //first attribute
		2, //how many components for the position?
		GL_FLOAT, //datatype 
		GL_FALSE, //should the data be normalized?
		sizeof(float)* 2, //size of whole vertex attribute stride for one primitive (3 * float for position + 3 * float for color)
		(GLvoid*)(0)); //offset in stride: position is begins at byte 0 in the stride. expected to be cast to GLvoid*


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	setupFBO();

	//clrmapper.values2TexCoord(displacementValues, renderPositionsAndValues, load.min_xdefValue, load.min_ydefValue, load.max_xdefValue, load.max_ydefValue);
	//std::for_each(renderPositionsAndValues.begin(), renderPositionsAndValues.end(), [](float x){ std::cout<<x<<std::endl;});
}

/////////////////////////////////////////////////////////////////////////////////////////


void cleanup()
{
	//glDeleteShader(vertexShader);
	//glDeleteShader(fragmentShader);
	//glDeleteProgram(shaderProgram);
	
	glDeleteBuffers(1, &vertexBufferObject);
	glDeleteBuffers(1, &elementArrayBuffer);

	glDeleteVertexArrays(1, &vertexArrayObject);

	//delete frame buffer objects 
	glDeleteFramebuffers(1, &fbo);
	glDeleteRenderbuffers(1, &depthBuffer);
}


/////////////////////////////////////////////////////////////////////////////////////////


void idleFunction(void)
{
	glutPostRedisplay();
}


/////////////////////////////////////////////////////////////////////////////////////////


void resizeFunction(int Width, int Height)
{
	windowWidth = Width;
	windowHeight = Height;
	glViewport(0, 0, windowWidth, windowHeight);

	//create a projection matrix 
	glm::mat4 projectionMatrix = glm::perspective(90.0f, //FOV 60.0�
		(float)windowWidth / windowHeight, //aspect ratio of the projection
		1.0f, //near clipping plane
		100.0f); //far clipping plane

	//##also upload projection matrix for our newly created star shader
	glUseProgram(circlesShaderProgram);
	glUniformMatrix4fv(circlesShaderProjectionMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
	glUseProgram(0);

	glUseProgram(meshShaderProgram);
	glUniformMatrix4fv(initPShaderProjectionMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
	glUseProgram(0);

	glUseProgram(defmeshShaderProgram);
	glUniformMatrix4fv(defPShaderProjectionMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
	glUseProgram(0);

	resizeFBOTextures();
}


/////////////////////////////////////////////////////////////////////////////////////////


void initWindow(int argc, char* argv[])
{
	glutInit(&argc, argv);

	glutInitContextVersion(3, 3);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutSetOption(
		GLUT_ACTION_ON_WINDOW_CLOSE,
		GLUT_ACTION_GLUTMAINLOOP_RETURNS
		);

	glutInitWindowSize(windowWidth, windowHeight);

	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);

	windowHandle = glutCreateWindow("");

	if(windowHandle < 1)
	{
		fprintf(
			stderr,
			"ERROR: Could not create a new rendering window.\n"
			);
		glutExit();
	}

	//GLUT function callbacks. This means: register functions which are called for the specific glut events. Mind the function signature!
	//??
	glutMouseFunc(mouseInput);
	//??
	glutSpecialFunc(specialKeyPress);
	//??
	glutSpecialUpFunc(specialKeyRelease);
	//??
	glutKeyboardFunc(keyPress);
	//??
	glutKeyboardUpFunc(keyRelease);

	//??
	glutTimerFunc(0, timerFunction, 0);
	//??
	glutReshapeFunc(resizeFunction);
	//??
	glutDisplayFunc(renderFunction);
	//??
	glutIdleFunc(idleFunction);
}

/////////////////////////////////////////////////////////////////////////////////////////

void initialize(int argc, char* argv[])
{
	GLenum GlewInitResult;

	initWindow(argc, argv);

	glewExperimental = GL_TRUE;
	//initialize glew for extensions
	GlewInitResult = glewInit();

	if (GLEW_OK != GlewInitResult)
	{
		fprintf(
			stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult)
			);
		glutExit();
	}

	fprintf(
		stdout,
		"INFO: OpenGL Version: %s\n",
		glGetString(GL_VERSION)
		);

	glGetError();
	//set color to clear the frame buffer with
	glClearColor(0.20f, 0.2f, 0.2f, 0.0f);

	//create shaders
	setupShader();
	//load model and fill buffer objects
	loadModel();
	loadTexture();
}

void setupFBO(void)
{
	//generate frame buffer
	glGenFramebuffers(1, &fbo);
	//bind the frame buffer
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	//create a texture resource
	glGenTextures(1, &colorBuffer);
	//bind it
	glBindTexture(GL_TEXTURE_2D, colorBuffer);

	//set the texture parameter
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//allocate texture memory for width*height RGBA8 pixel on the GPU
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, windowWidth, windowHeight, 0, GL_BGRA, GL_UNSIGNED_BYTE, NULL);

	//bind color attachment to framebuffer
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorBuffer, 0);

//create depth buffer
	//generate depth buffer
	glGenRenderbuffers(1, &depthBuffer);
	//bind depth buffer
	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	//reserve space
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, windowWidth, windowHeight);

	//bind depthbuffer to framebuffer
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);

	//unbind FBO
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


/////////////////////////////////////////////////////////////////////////////////////////
void resizeFBOTextures(void)
{
	glBindTexture(GL_TEXTURE_2D, colorBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, windowWidth, windowHeight, 0, GL_BGRA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, windowWidth, windowHeight);
}


void loadTexture()
{
	TextureLoader::loadImageToGLTexture(colorbar, "D:/repos/visualisierung_scivis/Framework SciVis/data/texture/colorbar_texture.png", GL_RGB8, GL_TEXTURE0);
}

void drawCircles(glm::mat4 const& model_matrix, glm::mat4 const& view_matrix)
{
	//##
	glUniformMatrix4fv(circlesShaderModelMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(model_matrix));

	//##bind star VertexArrayObject
	glBindVertexArray(circlesVertexArrayObject);

	//activate the texture
	//glActiveTexture(GL_TEXTURE0);
	//bind texture
	//glBindTexture(GL_TEXTURE_2D, colorbar);
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	//glDrawElements(GL_TRIANGLES, sizeof(int) * triangles.size(), GL_UNSIGNED_INT, 0);
  /// Set the color of all of this groups triangles based on 
  // color of this bucket.
	//glColorPointer(3, GL_FLOAT, 0, &stressx[0]);
	//glDrawElements(GL_TRIANGLES, sizeof(int) * triangles.size(), GL_UNSIGNED_INT, 0);
	for (int i = 0 ; i <= triangles.size(); i = i + 3)
	{
		//glColorPointer(3, GL_FLOAT, 0, &stressx[i]);
		glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, &triangles[i]);
	}

	

}

void drawInitMesh(glm::mat4 const& model_matrix, glm::mat4 const& view_matrix)
{
	glUniformMatrix4fv(initPShaderModelMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(model_matrix));
	glBindVertexArray(initPVertexArrayObject);
	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	glDrawElements(GL_TRIANGLES, sizeof(int) * triangles.size(), GL_UNSIGNED_INT, 0);
}

void drawDeformedMesh(glm::mat4 const& model_matrix, glm::mat4 const& view_matrix)
{
	glUniformMatrix4fv(defPShaderModelMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(model_matrix));
	glBindVertexArray(defPVertexArrayObject);
	
	glDrawElements(GL_TRIANGLES, sizeof(int) * triangles.size(), GL_UNSIGNED_INT, 0);
}
