#ifndef COLORMAPPER
#define COLORMAPPER
#include "Vector2.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <vector>
#include <array>

// fun stuff will happen here
// 2 coding schemes: one locally and one depending on the von Mises value for the material + locally

struct Color4f {
	Color4f(float red, float green, float blue, float a):
	r(red),
	g(green),
	b(blue),
	a(a){}
	float r, g, b, a;
	};

class ColorMapper
{
private:
	
public:
	int _sizeofinitpoints;
	float* newarray;
	ColorMapper(int sizeofinitpoints) {
		_sizeofinitpoints = sizeofinitpoints; 
		newarray = new float[_sizeofinitpoints * 2];};
	float map(float x, float in_min, float in_max, float out_min, float out_max);
	void values2TexCoord(std::vector<float> disp_Values, std::vector<float> renderPositions, float minx, float miny, float maxx, float maxy);

};
#endif