#version 330

in vec4 passed_normal;
in vec4 position;

uniform vec4 phongambient;
uniform vec4 phongdiffuse;
uniform vec4 phongspecular;
uniform int phongshine;
uniform vec4 phongcameraPos;

out vec4 phongout_Color;

void main(void)
{
	//normalize normal and light vector
	vec4 n = normalize(passed_normal);
	vec4 l = normalize(-position);
	//compute dot product
	float dot_n_l = max(dot(n, l), 0.0);

	//compute view vector
	vec4 v = normalize(position - phongcameraPos);
	//compute reflection vector
	vec4 r = normalize(2 * dot_n_l * n - l);
	//compute dot product
	float dot_v_r = max(dot(v, r), 0.0);
	
	//calculate final color
	phongout_Color = phongambient * 1.2f + phongdiffuse * 3.0f * dot_n_l + phongspecular * 0.2f * pow(dot_v_r, phongshine);
}
