#include "FileLoader.h"
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <algorithm>

unsigned int split(const std::string &txt, std::vector<std::string> &strs, char ch)
{
    unsigned int pos = txt.find( ch );
    unsigned int initialPos = 0;
    strs.clear();

    // Decompose statement
    while( pos != std::string::npos ) {
        strs.push_back( txt.substr( initialPos, pos - initialPos ) );
        initialPos = pos + 1;

        pos = txt.find( ch, initialPos );
    }

    // Add the last one
    strs.push_back( txt.substr( initialPos, std::min( pos, txt.size() ) - initialPos + 1 ) );

    return strs.size();
}


void FileLoader::readDisplacementValues(std::string filePath) 
{
	std::setprecision(20);
	std::cout.precision(20);
	std::cout<<std::fixed;
	std::fstream fin(filePath);
	char nextLine[100] = {0};
	std::vector<std::string> wordsinline;

	if (fin)
	{
		while (true)
		{
			//Read the next line from the file.
			fin.getline(nextLine, 100);
			std::string c=nextLine;
			//alllines.push_back(c);
			float x = std::atof(c.data());
			this->displacement_values.push_back(x);
			//Check to see if we've reached the end of the file. If so, break out of the reading/writing while loop.
			if (!fin)
			{
				break;
			}
			
		}
		
	}
	else
	{
		//The files were not opened properly - let the user know that it was unsuccessful.
		std::cout << "Error either opening \"input.txt\" for reading or opening \"output.txt\" for writing.";
	}

	fin.close();


	for (int i = 0; i < this->displacement_values.size(); i = i + 2)
	{
		if ( this->displacement_values[i] < this->min_xdefPoints) this->min_xdefPoints = this->displacement_values[i];
		if ( this->displacement_values[i] > this->max_xdefPoints) this->max_xdefPoints = this->displacement_values[i];

		if ( this->displacement_values[i+1] < this->min_ydefPoints) this->min_ydefPoints = this->displacement_values[i+1];
		if ( this->displacement_values[i+1] > this->max_ydefPoints) this->max_ydefPoints = this->displacement_values[i+1];
	}

}

void FileLoader::readStresses(std::string filePath) 
{
	std::setprecision(20);
	std::cout.precision(20);
	std::cout<<std::fixed;
	std::fstream fin(filePath);
	char nextLine[100] = {0};
	std::vector<std::string> wordsinline;

	if (fin)
	{
		while (true)
		{
			//Read the next line from the file.
			fin.getline(nextLine, 100);
			std::string c=nextLine;
			//alllines.push_back(c);
			float x = std::atof(c.data());
			this->stressx.push_back(x);
			//Check to see if we've reached the end of the file. If so, break out of the reading/writing while loop.
			if (!fin)
			{
				break;
			}
			
		}
		
	}
	else
	{
		//The files were not opened properly - let the user know that it was unsuccessful.
		std::cout << "Error either opening \"input.txt\" for reading or opening \"output.txt\" for writing.";
	}

	fin.close();
	this->stressx.pop_back();

	for (int i = 0; i < this->stressx.size(); i = i + 2)
	{
		if ( this->stressx[i] < this->min_stressx) this->min_stressx = this->stressx[i];
		if ( this->stressx[i] > this->max_stressx) this->max_stressx = this->stressx[i];
	}

}

void FileLoader::readDeformedPoints(std::string filePath)
{
	std::setprecision(2);
	std::cout.precision(2);
	std::fstream fin(filePath);
	char nextLine[100] = {0};
	std::vector<std::string> alllines;
	std::vector<std::string> wordsinline;
	if (fin)
	{
		
		while (true)
		{
			//Read the next line from the file.
			fin.getline(nextLine, 100);
			std::string c=nextLine;
			alllines.push_back(c);

			//Check to see if we've reached the end of the file. If so, break out of the reading/writing while loop.
			if (!fin)
			{
				break;
			}
			
		}
		
	}
	else
	{
		//The files were not opened properly - let the user know that it was unsuccessful.
		std::cout << "Error either opening \"input.txt\" for reading or opening \"output.txt\" for writing.";
	}

	fin.close();
	
	for (auto i=alllines.begin(); i != alllines.end() - 1; ++i)
	{
		
		//std::cout<<(*i)<<std::endl;
		//split by tab
		split((*i), wordsinline, '	');

		/*for (auto j = wordsinline.begin(); j!=wordsinline.end(); ++j)
		{
			std::cout<<(*j)<<std::endl;
		}*/

	
		float x = std::atof(wordsinline[0].data());
		float y = std::atof(wordsinline[1].data());
		this->renderPosition.push_back(x);
		this->renderPosition.push_back(y);

		if ( x < this->min_xdefPoints) this->min_xdefPoints = x;
		if ( x > this->max_xdefPoints) this->max_xdefPoints = x;
		if ( y < this->min_ydefPoints) this->min_ydefPoints = y;
		if ( y > this->max_ydefPoints) this->max_ydefPoints = y;
		//this->displacement_vectors.push_back(glm::vec2(x, y));

	}

	//std::for_each(this->renderPosition.begin(), this->renderPosition.end(), [](float x){std::cout<<x<<std::endl;});
	/*std::for_each(this->displacement_vectors.begin(), this->displacement_vectors.end(),
		[this](glm::vec2 a)
	{
		float renderMagnification = 1; // (2 / (0.3 / 18));
		renderPosition.push_back( float(a.x) * renderMagnification);
		renderPosition.push_back( float(a.y) * renderMagnification);
		std::cout<<float(a.x) * renderMagnification<<" "<<float(a.y) * renderMagnification<<std::endl;
	});*/


}

void FileLoader::readInitPoints(std::string filePath)
{
	std::setprecision(2);
	std::cout.precision(2);
	std::cout<<std::fixed;
	std::fstream fin(filePath);
	char nextLine[100] = {0};
	std::vector<std::string> alllines;
	std::vector<std::string> wordsinline;
	if (fin)
	{
		while (true)
		{
			//Read the next line from the file.
			fin.getline(nextLine, 100);
			std::string c=nextLine;
			alllines.push_back(c);

			//Check to see if we've reached the end of the file. If so, break out of the reading/writing while loop.
			if (!fin)
			{
				break;
			}
			
		}
		
	}
	else
	{
		//The files were not opened properly - let the user know that it was unsuccessful.
		std::cout << "Error either opening \"input.txt\" for reading or opening \"output.txt\" for writing.";
	}


	fin.close();
	
	for (auto i=alllines.begin(); i != alllines.end() - 1; ++i)
	{
		//std::cout<<(*i)<<std::endl;
		//split by tab
		split((*i), wordsinline, ',');

		/*for (auto j = wordsinline.begin(); j!=wordsinline.end(); ++j)
		{
			std::cout<<(*j)<<std::endl;
		}*/

	
		float x = std::atof(wordsinline[0].data());
		float y = std::atof(wordsinline[1].data());
		this->initPoints.push_back(x);
		this->initPoints.push_back(y);
		if ( x < this->min_xinitPoints) this->min_xinitPoints = x;
		if ( x > this->max_xinitPoints) this->max_xinitPoints = x;
		if ( y < this->min_yinitPoints) this->min_yinitPoints = y;
		if ( y > this->max_yinitPoints) this->max_yinitPoints = y;
		//this->displacement_vectors.push_back(glm::vec2(x, y));

	}

	//std::for_each(this->initPoints.begin(), this->initPoints.end(), [](float x){std::cout<<x<<std::endl;});
	/*std::for_each(this->initPoints.begin(), this->initPoints.end(),
		[this](float a)
	{
		float renderMagnification = (0.3 / 18);
		a = a * renderMagnification;
		std::cout<<a<<std::endl;
	});*/
	
	


}


void FileLoader::readTriangles(std::string filePath)
{
	std::fstream fin(filePath);
	char nextLine[100] = {0};
	std::vector<std::string> alllines;
	std::vector<std::string> wordsinline;
	if (fin)
	{
		while (true)
		{
			//Read the next line from the file.
			fin.getline(nextLine, 100);
			std::string c=nextLine;
			alllines.push_back(c);

			//Check to see if we've reached the end of the file. If so, break out of the reading/writing while loop.
			if (!fin)
			{
				break;
			}
			
		}
		
	}
	else
	{
		//The files were not opened properly - let the user know that it was unsuccessful.
		std::cout << "Error either opening \"input.txt\" for reading or opening \"output.txt\" for writing.";
	}


	fin.close();

	for (auto i=alllines.begin(); i!=alllines.end() - 1; ++i)
	{
		//split by tab
		split((*i), wordsinline, ',');
		
		int t1 = std::atof(wordsinline[0].data());
		int t2 = std::atof(wordsinline[1].data());
		int t3 = std::atof(wordsinline[2].data());
		std::array <int, 3> triangle;
		
		triangle[0] = t1 - 1;
		triangle[1] = t2 - 1;
		triangle[2] = t3 - 1;
		
		this->triangles.push_back(t1 - 1);
		this->triangles.push_back(t2 - 1);
		this->triangles.push_back(t3 - 1);
		

	}
	//::for_each(this->triangles.begin(), this->triangles.end(), [](int x){std::cout<<x<<std::endl;});
}
/*
void FileLoader::getMaxCoord_initPoints()
{
	for (auto i = this->initPoints.begin(); i != this->initPoints.end(); i = i + 2)
	{	if ((*i) > this->max_xinitPoints) this->max_xinitPoints = (*i);
	};

	for (auto i = this->initPoints.begin() + 1; i != this->initPoints.end(); i = i + 2)
	{	if ((*i) > this->max_yinitPoints) this->max_yinitPoints = (*i);
	};
}

void FileLoader::getMaxCoord_defPoints()
{
	for (auto i = this->renderPosition.begin(); i != this->renderPosition.end(); i = i + 2)
	{	if ((*i) > this->max_xdefPoints) this->max_xdefPoints = (*i);
	};

	for (auto i = this->renderPosition.begin() + 1; i != this->renderPosition.end(); i = i + 2)
	{	if ((*i) > this->max_ydefPoints) this->max_ydefPoints = (*i);
	};
}

void FileLoader::getMinCoord_initPoints()
{
	for (auto i = this->initPoints.begin(); i != this->initPoints.end(); i = i + 2)
	{	if ((*i) > this->min_xinitPoints) this->min_xinitPoints = (*i);
	};

	for (auto i = this->initPoints.begin() + 1; i != this->initPoints.end(); i = i + 2)
	{	if ((*i) > this->min_yinitPoints) this->min_yinitPoints = (*i);
	};
}

void FileLoader::getMinCoord_defPoints()
{
	for (auto i = this->renderPosition.begin(); i != this->renderPosition.end(); i = i + 2)
	{	if ((*i) > this->min_xdefPoints) this->min_xdefPoints = (*i);
	};

	for (auto i = this->renderPosition.begin() + 1; i != this->renderPosition.end(); i = i + 2)
	{	if ((*i) > this->min_ydefPoints) this->min_ydefPoints = (*i);
	};
}*/



