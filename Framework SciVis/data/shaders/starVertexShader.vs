#version 330

layout(location=0) in vec2 in_Position;
layout(location=1) in float s;
//layout(location=1) in vec2 tex_coords;

//Matrix Uniforms as specified with glUniformMatrix4fv
uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

out float s_;
//out vec2 tex_Coords;

void main(void)
{
	vec4 vertexPos = vec4(in_Position, 0.0, 1.0);
	//gl_Position = vec4(in_Position, 0.0, 1.0);
	//just set the position
	gl_Position = (ProjectionMatrix  * ViewMatrix * ModelMatrix) * vertexPos;
	s_ = s;
	//tex_Coords = tex_coords;
}
