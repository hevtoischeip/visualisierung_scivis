#version 330
//source: http://en.wikibooks.org/wiki/GLSL_Programming/Unity/Toon_Shading

in vec4 passed_normal;
in vec4 position;

uniform vec4 celrgb;
uniform vec4 celunlit_rgb;
uniform vec4 celoutline_rgb;
uniform vec4 celspecular;
uniform float celdiffuseThreshold;
uniform float cellitOutlineThickness;
uniform float celunlitOutlineThickness;
uniform int celshine;

uniform vec4 celcameraPos;

out vec4 out_Color;

void main(void)
{
	float distance = length(-position);
	float attenuation = 1.0f / distance;

	//default : unlit
	out_Color = celunlit_rgb;
	
	//low priority: diffuse - calculate normalized normal, light vector
	vec4 n = normalize(passed_normal);
	vec4 l = normalize(-position);
	//compute dot product
	float dot_n_l = max(dot(n, l), 0.0);
	//if the dot product is greater than the diffuse threshold, return color
	if (dot_n_l * attenuation >= celdiffuseThreshold)
	{
		out_Color = celrgb;
	}

	//higher priotity: outline
	//compute view vector
	vec4 v = normalize(position - celcameraPos);
	//compute dot product of view and normal vector
	float dot_v_n = max(dot(v, n), 0.0);
	//if the dot product is farther than the silhouette, the color is the outline color
	if ( dot_v_n > mix(celunlitOutlineThickness, cellitOutlineThickness, dot_n_l) )
	{
		out_Color = celoutline_rgb;
	}

	//highest priority: highlights
	//compute reflection vector
	vec4 r = normalize(2 * dot_n_l * n - l);
	//compute dot product of reflection and view vector
	float dot_v_r = max(dot(v, r), 0.0);
	//if the light source is on the right side and the intensity of the highlight is high, highlight
	if (dot_n_l > 0.0f && pow(dot_v_r, celshine) > 6.5f)
	{
		out_Color = celspecular * 3.2f * pow(dot_v_r, celshine);
	}


}
