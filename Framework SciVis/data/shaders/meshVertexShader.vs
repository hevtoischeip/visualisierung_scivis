#version 330

layout(location=0) in vec2 in_Position;

//Matrix Uniforms as specified with glUniformMatrix4fv
uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

out vec2 clr;

void main(void)
{
	vec4 vertexPos = vec4(in_Position, 0.0, 1.0);

	//just set the position
	gl_Position = (ProjectionMatrix  * ViewMatrix * ModelMatrix) * vertexPos;

	clr = vec2(1.0,0.0);
}
